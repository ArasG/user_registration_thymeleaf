package com.example.user_registration_thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserRegistrationThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserRegistrationThymeleafApplication.class, args);
    }

}

//TODO skirtingu spalvu navigacijos mugtukai
