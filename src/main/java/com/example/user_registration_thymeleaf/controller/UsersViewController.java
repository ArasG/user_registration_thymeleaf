package com.example.user_registration_thymeleaf.controller;

import com.example.user_registration_thymeleaf.model.User;
import com.example.user_registration_thymeleaf.model.UserRegistrationDTO;
import com.example.user_registration_thymeleaf.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


import javax.validation.Valid;
import java.util.Collections;


@Controller
public class UsersViewController {

    private final UserService userService;

    public UsersViewController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String returnGreetingPage(ModelMap modelMap) {
        modelMap.addAttribute("myName", "My name Aras");
        return "GreetingPage";
    }

    @GetMapping("/users")
    public String returnAllUsersView(ModelMap modelMap) {
        modelMap.addAttribute("users", userService.getAllUsers());
        return "AllUsersView";
    }

    @GetMapping("/register")
    public String returnRegistrationView(ModelMap modelMap) {
        modelMap.addAttribute("userRegistrationDTO", new UserRegistrationDTO());
        return "RegistrationView";
    }

    @PostMapping("/users")
    public String registerNewUser(@Valid UserRegistrationDTO userRegistrationDTO, BindingResult result, ModelMap modelMap) {
        if (result.hasErrors()){
            return "RegistrationView";
        }
        userService.userRegistration(userRegistrationDTO);
        return returnGreetingPage(modelMap);
    }

    @GetMapping ("/users/delete/{id}")
    public String deteleUser (@PathVariable Long id, ModelMap modelMap){
        userService.deleteUser(id);
        return returnAllUsersView(modelMap);
    }

    @GetMapping ("/users/edit/{id}")
    public String editUser (@PathVariable Long id, ModelMap modelMap){
        User user = userService.findById(id);
        UserRegistrationDTO userData = new UserRegistrationDTO();
        userData.setUserName(user.getUserName());
        userData.setEmail(user.getEmail());
        userData.setPassword(null);

        modelMap.addAttribute("userRegistrationDTO", userData);
        modelMap.addAttribute("id", id);

        return "EditView";
    }

    @PostMapping("/users/edit/{id}")
    public String editUser(@PathVariable Long id, @Valid UserRegistrationDTO userRegistrationDTO, BindingResult result, ModelMap modelMap) {
        if (result.hasErrors()){
            return "EditView";
        }
        userService.editUser(id, userRegistrationDTO);
        return returnGreetingPage(modelMap);
    }
}
