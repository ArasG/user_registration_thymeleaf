package com.example.user_registration_thymeleaf.repo;

import com.example.user_registration_thymeleaf.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public interface UserRepo extends JpaRepository<User, Long> {




}
