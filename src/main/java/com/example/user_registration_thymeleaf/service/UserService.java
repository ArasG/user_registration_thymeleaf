package com.example.user_registration_thymeleaf.service;

import com.example.user_registration_thymeleaf.model.User;
import com.example.user_registration_thymeleaf.model.UserRegistrationDTO;
import com.example.user_registration_thymeleaf.repo.UserRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserRepo userRepo;

    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public User userRegistration(UserRegistrationDTO userRegistrationDTO) {
        User user = new User();
        user.setUserName(userRegistrationDTO.getUserName());
        user.setEmail(userRegistrationDTO.getEmail());
        user.setPassword(String.valueOf(userRegistrationDTO.getPassword().hashCode()));
        return this.userRepo.save(user);
    }

    public List<User> getAllUsers(){
        return userRepo.findAll();
    }

    public void deleteUser(Long id) {
        userRepo.deleteById(id);
    }

    public User findById(Long id) {
        return userRepo.getById(id);
    }

    public void editUser(Long id, UserRegistrationDTO userRegistrationDTO) {
        User user = findById(id);
        user.setEmail(userRegistrationDTO.getEmail());
        user.setPassword(userRegistrationDTO.getPassword());
        user.setUserName(String.valueOf(userRegistrationDTO.getUserName().hashCode()));
        userRepo.save(user);
    }
}
